#include <iostream>
#include <vector>

using namespace std;

struct Point {
	int x;
	int y;
	int z;
};

struct Problem {
	vector<Point> points;
	int distance;
};

vector<Problem> get_input() {
	vector<Problem> retval;
	int num_points, distance;
	while (cin >> num_points && cin >> distance && num_points != 0) {
		Problem p;
		p.distance = distance;
		for (int i = 0; i < num_points; ++i) {
			int x, y, z;
			cin >> x >> y >> z;
			p.points.push_back(Point{x, y, z});
		}
		retval.push_back(p);
	}
	return retval;
}

bool close_enough(Point p1, Point p2, int r) {
	return (p1.x - p2.x)*(p1.x - p2.x) +\
	       (p1.y - p2.y)*(p1.y - p2.y) +\
		   (p1.z - p2.z)*(p1.z - p2.z) < r*r;
}


