#include <algorithm>
#include <vector>
#include <cmath>

using namespace std;

typedef unsigned int u32;

template<typename T>
T gcd(T a, T b) {
	while (b > 0) {
		// (a, b) = (b, a%b)
		T temp = a % b;
		a = b;
		b = temp;
	}
	return a;
}

template<typename T>
T expmod(T base, T pow, T mod) {
	if (pow == 0) {
		return 1;
	}
	if (pow % 2 == 0) {
		T sqrt_ans = expmod(base, pow/2, mod);
		return (sqrt_ans*sqrt_ans) % mod;
	} else {
		return (base * expmod(base, pow-1, mod)) % mod;
	}
}

vector<u32> sieve(u32 max_val) {
	// Numbers are stored as primes[0] -> 3, primes[1] -> 5, etc.
	// primes[n] -> 2n+3
	// primes[(x-3)/2] -> x
	size_t vec_size = (max_val - 1) / 2;
	vector<bool> primes (vec_size, true);
	size_t curr_idx = 0;
	u32 curr_prime = 3;
	while (curr_prime*curr_prime < max_val) {
		// Index corresponding to curr_prime^2
		size_t start_idx = 3+curr_idx*(6+curr_idx*(2));
		for (size_t i = start_idx; i < primes.size(); i += curr_prime) {
			primes[i] = false;
		}
		do {
			++curr_idx;
			curr_prime += 2;
		} while (primes[curr_idx] == false);
	}

	vector<u32> result;
	result.push_back(2);
	for (u32 i = 0; i < primes.size(); ++i) {
		if (primes[i]) {
			result.push_back(2*i+3);
		}
	}
	return result;
}

struct pfactor {
	u32 factor;
	u32 multiplicity;
};

template<size_t max_num>
vector<pfactor> prime_factors(u32 num) {
	vector<pfactor> retval;
	static vector<u32> primes = sieve(sqrt(max_num));
	for (size_t i = 0; i < primes.size() && primes[i]*primes[i] <= num; ++i) {
		u32 factor = primes[i];
		u32 multiplicity = 0;
		while (num%factor == 0) {
			++multiplicity;
			num /= factor;
		}
		if (multiplicity > 0) {
			retval.push_back(pfactor{factor, multiplicity});
		}
		if (num == 1) {
			break;
		}
	}
	if (num > 1) {
		retval.push_back(pfactor{num, 1});
	}
	return retval;
}
