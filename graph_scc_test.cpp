#include "graph.hpp"
#include "graph_scc.hpp"

#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

Graph read_graph() {
	int start, end;
	vector<DoubleEdge> edge_list;
	while (scanf("%d %d", &start, &end) == 2) {
		edge_list.push_back(DoubleEdge(start, end, 1));
	}
	return Graph(edge_list);
}

int main() {
	printf("Starting input\n");
	Graph g = read_graph();
	printf("Finished input.\n");
	printf("Starting DFS.\n");
	vector<int> sccs = scc(g);
	printf("Finished identifying SCCs.\n");
	printf("Calculating size of each.\n");
	vector<int> scc_sizes;
	for (int i = 0; i < sccs.size(); ++i) {
		if (scc_sizes.size() <= sccs[i]) {
			scc_sizes.resize(sccs[i]+1);
		}
		++scc_sizes[sccs[i]];
	}
	sort(scc_sizes.begin(), scc_sizes.end(), greater<int>());
	for (int i = 0; i < 20; ++i) {
		printf("%d\n", scc_sizes[i]);
	}
}
