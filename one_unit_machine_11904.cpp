#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

typedef long long lint;
#define MOD 1000000007L

typedef vector<lint> vl;

vector<vl> get_input() {
	lint total_cases;
	cin >> total_cases;
	vector<vl> result (total_cases);
	for (int i = 0; i < total_cases; ++i) {
		vl& nums = result[i];
		lint how_many;
		cin >> how_many;
		nums.resize(how_many);
		for (int j = 0; j < how_many; ++j) {
			cin >> nums[j];
		}
	}
	return result;
}

lint expmod(lint a, lint n) {
	// a^n mod p
	if (n == 1) {
		return a;
	}
	if (n%2 == 0) {
		return expmod(a*a%MOD, n/2);
	} else {
		return a*expmod(a, n-1) % MOD;
	}
}

lint inv(lint a) {
	// a^(-1) mod p == a^(p-2) mod p
	return expmod(a, MOD - 2);
}

lint choose(lint a, lint b) {
	lint acc = 1;
	if (b < a/2) {
		b = a - b;
	}
	for (int i = b+1; i <= a; ++i) {
		acc = (acc * i) % MOD;
	}
	lint dacc = 1;
	for (int i = 2; i <= a-b; ++i) {
		dacc = (dacc * i) % MOD;
	}
	acc = (acc * inv(dacc)) % MOD;
	return acc;
}

int solve_recur(vl& k_vec, int n) {
	if (n == 0) {
		return 1;
	}
	lint l = 0;
	for (int i = 0; i < n-1; ++i) {
		l += k_vec[i];
	}
	lint a = k_vec[n-1] - 1;
	lint fm = choose(l + a, a);
	return (fm * solve_recur(k_vec, n-1)) % MOD;
}

int solve(vl& k_vec) {
	return solve_recur(k_vec, k_vec.size());
}	

int main() {
	vector<vl> input = get_input();
	for (int i = 0; i < input.size(); ++i) {
		int res = solve(input[i]);
		printf("Case %d: %d\n", i+1, res);
	}
}
