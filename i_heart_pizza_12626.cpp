#include <iostream>
#include <map>
#include <cstdlib>
#include <string>
#include <sstream>

using namespace std;

map<char, int> get_input() {
  string buf;
  map<char, int> retval;
  getline(cin, buf);
  stringstream ss (buf);
  char c;
  while (ss >> c) {
    retval[c] += 1;
  }
  if (retval.empty()) {
    exit(0);
  }
  return retval;
}

int process(map<char, int>& char_freqs) {
  int min_val = 10000000;
  min_val = min(char_freqs['M'], min_val);
  min_val = min(char_freqs['A']/3, min_val);
  min_val = min(char_freqs['R']/2, min_val);
  min_val = min(char_freqs['G'], min_val);
  min_val = min(char_freqs['I'], min_val);
  min_val = min(char_freqs['T'], min_val);
  return min_val;
}

int main() {
  string x;
  getline(cin, x);
  while (1) {
    map<char, int> char_freqs = get_input();
    int num_pizzas = process(char_freqs);
    cout << num_pizzas << endl;
  }
}
