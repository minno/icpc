#pragma once

// General graph class

#include <vector>
#include <algorithm>
#include <limits>
#include <cassert>

using namespace std; // in a header, because fuck good practices

const float INF = std::numeric_limits<float>::infinity();

struct Edge {
	int other;
	float weight;

	Edge(int other, float weight)
		: other(other), weight(weight) {}
};

struct DoubleEdge {
	int src;
	int dst;
	float weight;

	DoubleEdge(int src, int dst, float weight)
		: src(src), dst(dst), weight(weight) {}
};

bool operator<(const Edge& lhs, const Edge& rhs) {
	return lhs.other < rhs.other;
}

vector<Edge>::const_iterator sorted_find(const vector<Edge>& v, int other) {
	return lower_bound(v.begin(), v.end(), Edge(other, 0));
}

vector<Edge>::iterator sorted_find(vector<Edge>& v, int other) {
	return lower_bound(v.begin(), v.end(), Edge(other, 0));
}

/** Inserts the given number into the vector, maintaining the invariant that it's a
  * sorted list of unique integers. Returns false if the element already exists.
*/
void sorted_insert(vector<Edge>& v, int other, float weight=1) {
	vector<Edge>::iterator it = sorted_find(v, other);
	if (it != v.end() && it->other == other) {
		it->weight = weight; // We're maintaining a set, so no duplicates allowed
	} else {
		v.insert(it, Edge(other, weight));
	}
}

void sorted_remove(vector<Edge>& v, int other) {
	vector<Edge>::iterator it = sorted_find(v, other);
	if (it != v.end() && it->other == other) {
		v.erase(it);
	}
}

/** Represents a graph where the vertices are labeled with the integers
  * 0, 1, 2, ... g.num_verts()-1
  * Supports quick insertion and querying of relationships and is relatively space-efficient.
  */
class Graph {
public:
	Graph(int size=0)
		: edges(size), edges_rev(size) {}

	Graph(const vector<DoubleEdge>& edge_list, bool undirected=false) {
		for (int i = 0; i < edge_list.size(); ++i) {
			DoubleEdge edge = edge_list[i];
			while (edge.src >= num_verts() || edge.dst >= num_verts()) {
				add_vertex();
			}
			edges[edge.src].push_back(Edge(edge.dst, edge.weight));
			edges_rev[edge.dst].push_back(Edge(edge.src, edge.weight));
			if (undirected) {
                edges[edge.dst].push_back(Edge(edge.src, edge.weight));
                edges_rev[edge.src].push_back(Edge(edge.dst, edge.weight));
            }
		}

		for (int i = 0; i < num_verts(); ++i) {
			sort(edges[i].begin(), edges[i].end());
			sort(edges_rev[i].begin(), edges_rev[i].end());
		}
	}

	void add_edge(int src, int dst, float weight=1) {
		assert(src >= 0 && dst >= 0 && src < num_verts() && dst < num_verts());
		sorted_insert(edges[src], dst, weight);
		sorted_insert(edges_rev[dst], src, weight);
	}

	void add_edge_undirected(int src, int dst, float weight=1) {
        add_edge(src, dst, weight);
        add_edge(dst, src, weight);
	}

	void remove_edge(int src, int dst) {
		assert(src >= 0 && dst >= 0 && src < num_verts() && dst < num_verts());
		sorted_remove(edges[src], dst);
		sorted_remove(edges_rev[dst], src);
	}

	int add_vertex() {
		edges.push_back(vector<Edge>());
		edges_rev.push_back(vector<Edge>());
		return num_verts() - 1;
	}

	int num_verts() const {
		return edges.size();
	}

	int num_edges() const {
		int total = 0;
		for (int i = 0; i < num_verts(); ++i) {
			total += edges[i].size();
		}
		return total;
	}

	double weight(int src, int dst, float _default=INF) const {
		assert(src >= 0 && dst >= 0 && src < num_verts() && dst < num_verts());
		vector<Edge>::const_iterator pos = sorted_find(edges[src], dst);
		if (pos == edges[src].end() || pos->other != dst) {
			return _default;
		}
		return pos->weight;
	}

	bool is_connected(int src, int dst) const {
		assert(src >= 0 && dst >= 0 && src < num_verts() && dst < num_verts());
		vector<Edge>::const_iterator pos = sorted_find(edges[src], dst);
		return pos != edges[src].end() && pos->other == dst;
	}

	const vector<Edge>& incoming(int dst) const {
		assert(dst >= 0 && dst < num_verts());
		return edges_rev[dst];
	}

	const vector<Edge>& outgoing(int src) const {
		assert(src >= 0 && src < num_verts());
		return edges[src];
	}

	vector<DoubleEdge> all_edges() const {
		vector<DoubleEdge> retval;
		for (int i = 0; i < num_verts(); ++i) {
			for (int j = 0; j < edges[i].size(); ++j) {
				retval.push_back(DoubleEdge(i, edges[i][j].other, edges[i][j].weight));
			}
		}
		return retval;
	}

	void reverse() {
		std::swap(edges, edges_rev);
	}

	vector<vector<Edge> > edges;
	vector<vector<Edge> > edges_rev;
};
