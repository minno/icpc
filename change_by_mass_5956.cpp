#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

struct Problem {
	int cents;
	bool has_halfdollar;
};

vector<Problem> get_input() {
	int cents, num_halfdollars;
	vector<Problem> retval;
	Problem p;
	while (cin >> cents && cents != -1 && cin >> num_halfdollars) {
		p.cents = cents;
		p.has_halfdollar = num_halfdollars > 0;
		retval.push_back(p);
	}
	return retval;
}

struct Solution {
	int num_halfdollars;
	int num_quarters;
	int num_dimes;
	int num_nickels;
	int num_pennies;
};

Solution solve(Problem p) {
	Solution retval;
	retval.num_halfdollars = 0;
	retval.num_quarters = p.cents / 25;
	p.cents %= 25;
	retval.num_dimes = p.cents / 10;
	p.cents %= 10;
	retval.num_nickels = p.cents / 5;
	p.cents %= 5;
	retval.num_pennies = p.cents;

	while (retval.num_quarters > 0 && retval.num_nickels > 0) {
		retval.num_dimes += 3;
		retval.num_quarters -= 1;
		retval.num_nickels -= 1;
	}

	if (retval.num_quarters >= 2 && p.has_halfdollar) {
		retval.num_quarters -= 2;
		retval.num_halfdollars = 1;
	}
	
	return retval;
}

int main() {
	vector<Problem> input = get_input();
	for (int i = 0; i < input.size(); ++i) {
		Solution s = solve(input[i]);
		printf("Request %d:", i+1);
		if (s.num_halfdollars > 0) {
			printf(" %dx50", s.num_halfdollars);
		}
		if (s.num_quarters > 0) {
			printf(" %dx25", s.num_quarters);
		}
		if (s.num_dimes > 0) {
			printf(" %dx10", s.num_dimes);
		}
		if (s.num_nickels > 0) {
			printf(" %dx5", s.num_nickels);
		}
		if (s.num_pennies > 0) {
			printf(" %dx1", s.num_pennies);
		}
		printf("\n");
	}
}
