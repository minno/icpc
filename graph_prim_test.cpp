#include "graph.hpp"
#include "graph_prim.hpp"

#include <cstdio>

using namespace std;

Graph get_input() {
    int start, end, weight;
    vector<DoubleEdge> edges;
    scanf("%d %d", &start, &end); // Throw away header
    while (scanf("%d %d %d", &start, &end, &weight) == 3) {
        edges.push_back(DoubleEdge(start-1, end-1, float(weight)));
    }
    return Graph(edges, true);
}

int main() {
    Graph g = get_input();
    vector<DoubleEdge> mst = prim_mst(g);
    float sum = 0;
    for (int i = 0; i < mst.size(); ++i) {
        sum += mst[i].weight;
    }
    printf("MST edge count: %d\n", mst.size());
    printf("Total mst weight: %f\n", sum);
}
