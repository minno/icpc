#include <iostream>
#include <cstdio>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

bool is_palindrome(int num) {
	int reversed_num = 0;
	int scratch = num;

	while (scratch > 0) {
		reversed_num *= 10;
		reversed_num += scratch%10;
		scratch /= 10;
	}

	return num == reversed_num;
}

vector<int> get_input() {
	vector<int> retval;
	int num;
	while ((cin >> num) && num != -1) {
		retval.push_back(num);
	}
	return retval;
}

int next_palindrome(int num) {
	while (!is_palindrome(num)) {
		num = (num+1)%1000000;
	}
	return num;
}

int main() {
	vector<int> input = get_input();
	for (int i = 0; i < input.size(); ++i) {
		int next = next_palindrome(input[i]);
		printf("Case %d: %d miles to %06d.\n", i+1, (next-input[i]+1000000)%1000000, next);
	}
}
