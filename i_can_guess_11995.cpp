#include <iostream>
#include <vector>
#include <string>

#include <stack>
#include <queue>

using namespace std;

struct op {
	int type;
	int value;
};

typedef vector<op> vo;

vector<vo> get_input() {
	vector<vo> result;
	while (1) {
		int num_ops;
		cin >> num_ops;
		if (!cin) {
			return result;
		}
		vo ops (num_ops);
		for (int i = 0; i < num_ops; ++i) {
			cin >> ops[i].type >> ops[i].value;
		}
		result.push_back(ops);
	}
}

string solve(vo ops) {
	vector<int> s;
	queue<int> q;
	priority_queue<int> p;
	bool s_valid = true;
	bool q_valid = true;
	bool p_valid = true;
	for (int i = 0; i < ops.size(); ++i) {
		int type = ops[i].type;
		int value = ops[i].value;
		if (type == 1) {
			s.push_back(value);
			q.push(value);
			p.push(value);
		} else {
			if (s.empty() || (s_valid && s.back() != value)) {
				s_valid = false;
			} else {
				s.pop_back();
			}
			if (q.empty() || (q_valid && q.front() != value)) {
				q_valid = false;
			} else {
				q.pop();
			}
			if (p.empty() || (p_valid && p.top() != value)) {
				p_valid = false;
			} else {
				p.pop();
			}
		}
	}
	if ((s_valid && p_valid) || (s_valid && q_valid) || (p_valid && q_valid)) {
		return "not sure";
	} else if (!s_valid && !p_valid && !q_valid) {
		return "impossible";
	} else if (s_valid) {
		return "stack";
	} else if (p_valid) {
		return "priority queue";
	} else if (q_valid) {
		return "queue";
	}
}

int main() {
	vector<vo> input = get_input();
	for (int i = 0; i < input.size(); ++i) {
		string s = solve(input[i]);
		cout << s << endl;
	}
}	
