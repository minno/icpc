#pragma once

// SCC algorithm
#include "graph.hpp"
#include <stack>
#include <vector>

// Returns a vector of integers such that retval[i] == retval[j] iff vertices
// i and j are strongly connected.
vector<int> scc(const Graph& g) {

    // First, get a stack of vertices such that, when you construct the DAG of
    // SCCs, the ones downstream occur above the ones upstream.
    // This is done by doing a DFS on the reversed graph, and pushing the vertices
    // in the order that the search bottoms out, which will put the upstream ones
    // on the stack first.
    stack<int> ordered_vertices;
    {
        vector<bool> explored (g.num_verts(), false);
        stack<int> todo;

        for (int vertex_num = 0; vertex_num < g.num_verts(); ++vertex_num) {
            if (!explored[vertex_num]) {
                todo.push(vertex_num);
                explored[vertex_num] = true;
            }
            while (!todo.empty()) {
                int next_vertex = todo.top();
                bool any_pushed = false;
                const vector<Edge>& adj_vertices = g.incoming(next_vertex);
                for (int i = 0; i < adj_vertices.size(); ++i) {
                    int dst = adj_vertices[i].other;
                    if (!explored[dst]) {
                        explored[dst] = true;
                        todo.push(dst);
                        any_pushed = true;
                    }
                }
                if (!any_pushed) {
                    ordered_vertices.push(next_vertex);
                    todo.pop();
                }
            }
        }
    }

    // Then, using that stack, do a second series of DFSs that fill
    // in each SCC
    {
        vector<int> retval(g.num_verts());
        vector<bool> explored(g.num_verts());
        vector<bool> seen(g.num_verts());

        int curr_label = 0;
        while(!ordered_vertices.empty()) {
            int v = ordered_vertices.top();
            ordered_vertices.pop();
            if (explored[v]) {
                continue;
            }
            stack<int> todo;
            todo.push(v);
            seen[v] = true;
            while(!todo.empty()) {
                int curr = todo.top();
                todo.pop();
                retval[curr] = curr_label;
                explored[curr] = true;
                const vector<Edge>& adj = g.outgoing(curr);
                for (int i = 0; i < adj.size(); ++i) {
                    int dest = adj[i].other;
                    if (!seen[dest]) {
                        seen[dest] = true;
                        todo.push(dest);
                    }
                }
            }
            ++curr_label;
        }

        return retval;
    }
}

