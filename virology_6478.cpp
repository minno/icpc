#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

vector<vector<int> > get_input() {
	int num_inputs;
	cin >> num_inputs;
	vector<vector<int> > retval (num_inputs, vector<int>(14));
	for (int i = 0; i < num_inputs; ++i) {
		for (int j = 0; j < 14; ++j) {
			cin >> retval[i][j];
		}
	}

	return retval;
}

bool isnt_immune(vector<int>& sequence) {
	sort(sequence.begin(), sequence.end());

	for (int pair_offset = 0; pair_offset <= 12; pair_offset += 3) {
		if (sequence[pair_offset] != sequence[pair_offset+1]) {
			continue;
		}
		bool all_triples_ok = true;
		for (int triple_loc = 0; triple_loc <= 11; triple_loc += 3) {
			if (triple_loc == pair_offset) {
				triple_loc += 2;
			}
			if ((sequence[triple_loc] != sequence[triple_loc+1] || sequence[triple_loc+1] != sequence[triple_loc+2])
	&& (sequence[triple_loc]+2 != sequence[triple_loc+1]+1 || sequence[triple_loc+1]+1 != sequence[triple_loc+2])) {
				all_triples_ok = false;
				break;
			}
		}
		if (all_triples_ok) {
			return true;
		}
	}
	return false;
}

int main() {
	vector<vector<int> > input = get_input();
	for (int i = 0; i < input.size(); ++i) {
		if (!isnt_immune(input[i])) {
			cout << "Immune\n";
		} else {
			cout << "Vulnerable\n";
		}
	}
}
