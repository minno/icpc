#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <sstream>

using namespace std;

long long next(long long num) {
	if (num%2 == 0) {
		return num/2;
	} else {
		return 3*num + 1;
	}
}

long long cycle_length(long long num) {
	long long count = 1;
	while (num != 1) {
		num = next(num);
		++count;
	}
	return count;
}

vector<pair<int, int> > get_input() {
	string buf;
	vector<pair<int, int> > pairs;
	while (getline(cin, buf)) {
		stringstream s (buf);
		int x, y;
		s >> x;
		s >> y;
		pairs.push_back(make_pair(x, y));
	}
	return pairs;
}

long long max_cycle(long long first, long long last) {
	if (first > last) {
		swap(first, last);
	}
	long long i = first;
	long long max_val = 0;
	for (; i <= last; ++i) {
		long long len = cycle_length(i);
		if (len > max_val) {
			max_val = len;
		}
	}
	return max_val;
}

int main() {
	vector<pair<int, int> > input = get_input();
	for (auto p : input) {
		cout << p.first << ' ' << p.second << ' ' << max_cycle(p.first, p.second) << endl;
	}
}
