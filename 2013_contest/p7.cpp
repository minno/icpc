#include <iostream>
#include <cstdio>

using namespace std;

struct point {
	double x;
	double y;
	double m;
};

//vector<point> get_one_input() {
//	int num_points;
//	cin >> num_points;
//	if (num_points < 0) {
//		return vector<point>{};
//	}
//	vector<point> points (num_points);
//	for (int i = 0; i < num_points; ++i) {
//		cin >> points[i].x >> points[i].y >> points[i].m;
//	}
//	return points;
//}

int main() {
	int case_num = 1;
	try {
		while (1) {
			int num_points;
			cin >> num_points;
			double mass_sum = 0;
			double xm_sum = 0;
			double ym_sum = 0;
			if (num_points <= 0) {
				break;
			}
			for (int i = 0; i < num_points; ++i) {
				point p;
				cin >> p.x >> p.y >> p.m;
				mass_sum += p.m;
				xm_sum += p.m * p.x;
				ym_sum += p.m * p.y;
			}
			printf("Case %d: %.2f %.2f\n", case_num, xm_sum/mass_sum, ym_sum/mass_sum);
			++case_num;
		}
	} catch (ios_base::failure) {
	}
}
