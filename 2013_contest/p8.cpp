#include <iostream>
#include <algorithm>
#include <vector>

#include <string>

using namespace std;

struct spair {
	string first;
	string second;
};

spair get_one_input() {
	spair sp;
	cin >> sp.first >> sp.second;
	return sp;
}

bool is_anagram(string a, string b) {
	if (a == b) {
		return true;
	}
	if (a.size() != b.size()) {
		return false;
	}
	sort(a.begin(), a.end());
	sort(b.begin(), b.end());
	return (a == b);
}

int main() {
	for (int i = 0; ; ++i) {
		spair sp = get_one_input();
		if (sp.first == "END" && sp.second == "END") {
			break;
		}
		string s = (is_anagram(sp.first, sp.second))?"same":"different";
		cout << "Case " << (i+1) << ": " << s << '\n';
	}
}
