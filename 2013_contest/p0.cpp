#include <iostream>

using namespace std;

int main() {
	int a, b, c;
	int max_rem = 0;
	while (cin >> a >> b >> c) {
		int rem = (a+b)%c;
		if (rem > max_rem) {
			max_rem = rem;
		}
	}
	cout << max_rem << '\n';
}
