#include <iostream>
#include <string>
#include <vector>

#include <set>
#include <deque>
#include <functional>
#include <vector>

using namespace std;

class Graph {
public:
	int max;

	Graph (int max_val) : max(max_val), edges(max_val) {}

	void insert_edge(int a, int b) {
		edges[a].insert(b);
	}

	void remove_edge(int a, int b) {
		edges[a].erase(b);
	}

	void has_edge(int a, int b) {
		return edges[a].find(b) != edges[a].end();
	}

	const set<int>& all_edges(int a) {
			return edges[a];
	}

	void dfs(int start, function<void(int)> callback) {
			search_impl<true>(start, callback);
	}

	void bfs(int start, function<void(int)> callback) {
			search_impl<false>(start, callback);
	}

private:
	template<bool is_dfs>
	void search_impl(int start_edge, function<void(int)> callback) {
			deque<int> verts;
			verts.push_back(start_edge);
			vector<bool> visited (max, false);
			visited[start_edge] = true;
			while (!verts.empty()) {
				int v;
				if (is_dfs) {
					v = verts.back();
					verts.pop_back();
				} else {
					v = verts.front();
					verts.pop_front();
				}
				callback(v);
				for (const int i : all_edges(v)) {
					if (!visited[i]) {
						verts.push_back(i);
						visited[i] = true;
					}
				}
			}
	}

	vector<set<int>> edges;
};

