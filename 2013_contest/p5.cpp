#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

bool is_square(long long n) {
	// N <= 2^54
	long long min = 0;
	long long max = (1L << 31);
	while ((max - min) > 1) {
		long long guess = min/2 + max/2 + (min & max & 1);
		if (guess * guess > n) {
			max = guess;
		} else if (guess*guess == n) {
			return true;
		} else {
			min = guess;
		}
	}
	if (max*max == n || min*min == n) {
		return true;
	} else {
		return false;
	}
}

int main() {
	while (1) {
		long long n;
		cin >> n;
		cout << is_square(n) << endl;
	}
}
