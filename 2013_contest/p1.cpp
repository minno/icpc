#include <iostream>
#include <vector>

using namespace std;

struct frame {
	int first;
	int second;
};

struct partial_game {
	vector<frame> frames;

	int max_score () {
		int score = 60;
		// Pins
		for (int i = 0; i < frames.size(); ++i) {
			score += frames[i].first + frames[i].second;
		}
		// Process spares
		for (int i = 0; i < frames.size(); ++i) {
			if (frames[i].first + frames[i].second == 10 && frames[i].first != 10) {
				if (i == 7) {
					score += 10;
				} else {
					score += frames[i+1].first;
				}
			}
		}

		// Process strikes
		for (int i = 0; i < frames.size(); ++i) {
			if (frames[i].first == 10) {
				if (i == 7) {
					score += 20;
				} else if (i == 6) {
					if (frames[7].first != 10) {
						score += frames[7].first + frames[7].second;
					} else {
						score += 20;
					}
				} else {
					if (frames[i+1].first != 10) {
						score += frames[i+1].first + frames[i+1].second;
					} else {
						score += frames[i+1].first + frames[i+2].first;
					}
				}
			}
		}

		return score;
	}

	int max_score_with_double_gutter() {
		return score_with_gutters() + 30;
	}

	int score_with_gutters() {
		int score = 0;

		// Pins
		for (int i = 0; i < frames.size(); ++i) {
			score += frames[i].first + frames[i].second;
		}

		// Process spares
		for (int i = 0; i < frames.size(); ++i) {
			if (frames[i].first + frames[i].second == 10 && frames[i].first != 10) {
				if (i == 7) {
					score += 0;
				} else {
					score += frames[i+1].first;
				}
			}
		}

		// Process strikes
		for (int i = 0; i < frames.size(); ++i) {
			if (frames[i].first == 10) {
				if (i == 7) {
					score += 0;
				} else if (i == 6) {
					if (frames[7].first != 10) {
						score += frames[7].first + frames[7].second;
					} else {
						score += 10;
					}
				} else {
					if (frames[i+1].first != 10) {
						score += frames[i+1].first + frames[i+1].second;
					} else {
						score += frames[i+1].first + frames[i+2].first;
					}
				}
			}
		}

		return score;
	}
};

partial_game read_one_game() {
	partial_game g;
	g.frames.resize(8);
	int i = 0;
	while (i < 8) {
		cin >> g.frames[i].first;
		if (g.frames[i].first == 10) {
			g.frames[i].second = 0;
		} else {
			cin >> g.frames[i].second;
		}
		++i;
	}
	return g;
}

int main() {
	int case_num = 1;
	while (1) {
		int goal_score;
		cin >> goal_score;
		if (goal_score < 0) {
			break;
		}
		cout << "Case " << case_num++ << ": ";
		partial_game g = read_one_game();
		if (goal_score >= g.max_score()) {
			cout << "impossible";
		} else if (goal_score < g.score_with_gutters()) {
			cout << "0 0 0 0";
		} else if (goal_score < g.max_score_with_double_gutter()) {
			cout << "0 0 ";
			int base_score = g.score_with_gutters();
			int score_diff = goal_score - base_score;
			if (score_diff <= 9) {
				cout << "0 " << (score_diff + 1);
			} else if (score_diff <= 19) {
				cout << "0 10 " << (score_diff - 10 + 1);
			} else {
				cout << "10 10 " << (score_diff - 20 + 1);
			}
		} else {
			int score_diff = goal_score - g.max_score_with_double_gutter();
			if (score_diff <= 9) {
				if (g.frames[7].first == 10) {
					cout << "0 " << (score_diff/2 + 1) << "10 10 10";
				} else {
					cout << "0 " << (score_diff + 1) << "10 10 10";
				}
			} else if (score_diff <= 19) {
				if (g.frames[7].first == 10) {
					cout << "0 " << ((score_diff - 10)/2 + 1) << "10 10 10";
				} else {
					cout << "0 10 10 " << (score_diff-10 + 1) << " 10";
				}
			} else if (score_diff <= 29) {
				if (g.frames[7].first == 10) {
					cout << "0 10 10 " << (score_diff-20 + 1) << " 10";
				} else if (g.frames[7].first + g.frames[7].second == 10) {
					cout << (score_diff - 20 + 1) << (10 - ((score_diff - 20 + 1))) << "10 10 10";
				} else {
					cout << "10 10 " << ((score_diff-20)/2 + 1) << " 10";
				}
			} else if (score_diff <= 39) {
				if (g.frames[7].first == 10) {
					if (g.frames[6].first == 10) {
						cout << (score_diff - 30 + 1) << (10 - (score_diff - 30 + 1)) << "10 10 10";
					} else {
						cout << "10 10 " << ((score_diff-30)/2 + 1) << " 10";
					}
				} else if (g.frames[7].first + g.frames[7].second == 10) {
					cout << "10 10 " << ((score_diff-30)/2 + 1) << " 10";
				}
			} else if (score_diff <= 49) {
				cout << "10 10 10 " << (score_diff-40 + 1);
			} else {
				cout << "10 10 10 10";
			}
		}
		cout << endl;
	}
}
