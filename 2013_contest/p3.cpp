#include <iostream>
#include <utility>
#include <string>

using namespace std;

struct pos {
	int n;
	char from;
	char to;
};

pos find_move(int n, long long k, char from, char spare, char target) {
	while (true) {
		if (k < (1L << (n-1))) {
			n = n-1;
			swap(spare, target);
		} else if (k == (1L << (n - 1))) {
			pos p;
			p.n = n;
			p.from = from;
			p.to = target;
			return p;
		} else {
			n = n-1;
			k = k - (1L << n);
			swap(from, spare);
		}
	}
}

void solve(int n, long long k) {
	pos p = find_move(n, k, 'A', 'B', 'C');
	cout << p.n << " " << p.from << " " << p.to << '\n';
}

int main() {
	try {
		int n;
		long long k;
		int i = 1;
		while (cin >> k >> n) {
			if (n == 0 && k == 0) {
				break;
			}
			cout << "Case " << i << ": ";
			solve(n, k);
			++i;
		}
	} catch (...) {
		cerr << "Failed something, terminating" << endl;
	}
	return 0;
}
