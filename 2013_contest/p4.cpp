#include <iostream>

#include <vector>
#include <deque>
#include <utility>

using namespace std;

//struct tower {
//	vector<vector<int>> pins (4);
//
//	tower(n) {
//		for (int i = 0; i < n; ++i) {
//			pins[0].push_back(i);
//		}
//	}
//
//	bool can_move(int from, int to) {
//		return (pins[pina] < pins[pinb]);
//	}
//
//	bool move(int from, int to) {
//		if (can_move(from, to) {
//			int disk = pins[from].back();
//			pins[to].push_back(disk);
//			pins[from].pop_back();
//			return true;
//		} else {
//			return false;
//		}
//	}
//
//	bool
//}
//
//long long num_moves_brute(tower t) {
//	deque<pair<tower, int>> queue;
//
//
//long long num_moves_recur(long long n) {
//	if (n == 1) {
//		return 1;
//	} else if (n == 2) {
//		return 3;
//	} else {
//		return 2*num_moves_recur(n-2) + 3;
//	}
//}
//
//long long num_moves(long long n) {
//	if (n%2 == 0) {
//		long long acc = 3;
//		for (int i = 2; i < n; i += 2) {
//			acc *= 2;
//			acc += 3;
//		}
//		return acc;
//	} else {
//		long long acc = 1;
//		for (int i = 1; i < n; i += 2) {
//			acc *= 2;
//			acc += 3;
//		}
//		return acc;
//	}
//}

long long solve(int n) {
	long long res = 0;
	int count = 0;
	for (int step_size_pow = 0; step_size_pow < 50; step_size_pow++) {
		long long step_size = (1L << step_size_pow);
		for (long long i = 0; i < 1+step_size_pow; ++i) {
			if (count == n) {
				return res;
			}
			++count;
			res += step_size;
		}
	}
	return -1;
}

int main() {
	int input;
	int case_num = 1;
	while (cin >> input) {
		long long res = solve(input);
		cout << "Case " << case_num++ << ": " << res << endl;
	}
}
