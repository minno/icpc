#include <iostream>
#include <vector>

using namespace std;

struct Node {
	int id;
	int parent_index;
	vector<int> children_indices;
};

struct Problem {
	vector<Node> nodes;
	int interesting_node;
};

const int MAX_ID = 1000001;

vector<Problem> get_input() {
	vector<Problem> retval;
	while (true) {
		int num_nodes, interesting_node;
		cin >> num_nodes >> interesting_node;
		if (num_nodes == 0) {
			return retval;
		}
		vector<int> nums;
		int n;
		for (int i = 0; i < num_nodes; ++i) {
			cin >> n;
			nums.push_back(n);
		}

		vector<Node> nodes;
		nodes.push_back(Node{nums[0], -1, vector<int>()});
		int curr_parent_index = -1;
		for (int i = 1; i < nums.size(); ++i) {
			if (nums[i-1] + 1 != nums[i]) {
				// New contiguous sequence
				++curr_parent_index;
			}
			Node new_node;
			new_node.id = nums[i];
			new_node.parent_index = curr_parent_index;
			new_node.children_indices = vector<int>();
			nodes[curr_parent_index].children_indices.push_back(i);
			nodes.push_back(new_node);
		}	

		retval.push_back(Problem{nodes, interesting_node});
	}
}

int solve(Problem& problem) {
	int node_loc;
	vector<Node>& nodes = problem.nodes;
	for (int i = 0; i < nodes.size(); ++i) {
		if (nodes[i].id == problem.interesting_node) {
			node_loc = i;
			break;
		}
	}

	if (nodes[node_loc].parent_index == -1) {
		return 0;
	}

	int grandparent_loc = nodes[nodes[node_loc].parent_index].parent_index;
	
	if (grandparent_loc < 0) {
		return 0;
	}

	vector<int>& parents = nodes[grandparent_loc].children_indices;
	int num_cousins = 0;
	for (int i = 0; i < parents.size(); ++i) {
		num_cousins += nodes[parents[i]].children_indices.size();
	}

	int parent_loc = nodes[node_loc].parent_index;
	num_cousins -= nodes[parent_loc].children_indices.size();
	return num_cousins;
}

int main() {
	vector<Problem> input = get_input();
	for (int i = 0; i < input.size(); ++i) {
		cout << solve(input[i]) << endl;
	}
}
