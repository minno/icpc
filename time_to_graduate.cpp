#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <string>
#include <utility>
#include <sstream>

using namespace std;

struct clss {
  string name;
  map<string, bool> is_prereq;
  bool is_fall;
  bool is_spring;
};

vector<clss> all_classes;
int max_classes;

void get_input() {
  int a;
  char c;
  string line_buf, buf;
  all_classes.clear();
  cin >> a >> max_classes;
  if (a == -1 && b == -1) {
    throw "done";
  }
  clss cs;
  getline(line_buf); // consume extra
  for (int i = 0; i < a; ++i) {
    getline(line_buf);
    stringstream s (line_buf);
      
    buf >> cs.name;

    buf >> c;
    switch (c) {
    case 'F':
      cs.is_fall = true;
      cs.is_spring = false;
      break;
    case 'S':
      cs.is_fall = false;
      cs.is_spring = true;
      break;
    case 'B':
      cs.is_fall = cs.is_spring = true;
      break;
    }

    while (cin >> buf2) {
      cs.prereqs.push_back(buf2);
    }
  }
  all_classes.push_back(cls);
}

bool can_take_fall(const clss& course) {
  return (course.prereqs.empty() && course.is_fall);
}

bool can_take_spring(const clss& course) {
  return (course.prereqs.empty() && course.is_spring);
}

int max_sems(vector<clss> classes, bool is_fall = true) {
  vector<clss> to_take;
  if (classes.empty()) {
    return 0;
  }
  for (vector<clss>::iterator i = classes.begin(); i != classes.end(); ) {
    if ((is_fall && can_take_fall(*i)) || (is_spring && can_take_spring(*i))) {
      to_take.push_back(*i);
      i = classes.erase(i);
    } else {
      ++i;
    }
  }
  if (to_take.size() <= max_classes) {
    if (classes.empty()) {
      return 1;
    } else {
      for (vector<clss>::iterator i = classes.begin(); i != classes.end(); ++i) {
	

int main() {
  try {
    while (1) {
      get_input();
      int num = max_sems(all_classes);
      cout << "The minimum number of semesters required to graduate is " << num << '.' << endl;
    }
  } catch (const char* msg) {
  }
}