#include <vector>
#include <set>

using namespace std;

class Graph {
public:
	Graph (int max_val) : edges(max_val) {}

	void insert_edge(int a, int b) {
		edges[a].insert(b);
	}

	void remove_edge(int a, int b) {
		edges[a].erase(b);
	}

	bool has_edge(int a, int b) {
		return edges[a].find(b) != edges[a].end();
	}

	const set<int>& all_edges(int a) {
		return edges[a];
	}

private:
	vector<set<int> > edges;
};

#include <iostream>

vector<int> orig_husbands;

Graph get_input() {
	int num_men;
	cin >> num_men;
	Graph result (num_men);
	for (int i = 0; i < num_men; ++i) {
		int num_women;
		cin >> num_women;
		for (int j = 0; j < num_women; ++j) {
			int woman_num;
			cin >> woman_num;
			result.insert_edge(i, woman_num);
		}
	}
	orig_husbands.resize(num_men);
	for (int i = 0; i < num_men; ++i) {
		int woman;
		cin >> woman;
		orig_husbands[woman] = i;
	}
	return result;
}

bool can_remove_pair(Graph& graph, int man, int woman) {
	if (orig_husbands[woman] = man) {
		return true;
	}
}

int main() {
	Graph g = get_input();

}
