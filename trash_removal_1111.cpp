#include <iostream>
#include <vector>
#include <cmath>
#include <utility>
#include <cstdio>

using namespace std;

struct point {
	double x;
	double y;
};

typedef vector<point> vp;
typedef vector<vp> polys;

polys get_input() {
	polys retval;
	while (true) {
		int num_points;
		cin >> num_points;
		if (num_points == 0) {
			return retval;
		}
		vp points (num_points);
		for (int i = 0; i < num_points; ++i) {
			cin >> points[i].x >> points[i].y;
		}
		retval.push_back(points);
	}
}

double distance(point a, point p1, point p2) {
	if (abs(p2.y - p1.y) > abs(p2.x - p1.x)) {
		swap(p1.x, p1.y);
		swap(p2.x, p2.y);
		swap(a.x, a.y);
	}
	double m = (p2.y - p1.y) / (p2.x - p1.x);
	double b = p1.y - m*p1.x;
	return (a.y - m*a.x - b) / sqrt(m*m + 1);
}

double process(vp points) {
	// Returns the min width of the shape.
	int n = points.size();
	double min_width = (1 << 30);
	for (int i = 0; i < n; ++i) {
		// i is the index of the starting vertex we're working with.
		for (int j = i+1; j < n; ++j) {
			// j is the index of the other endpoint of the test edge
			int working_sign = 0; // -1 if we've found negative distance edges, 1 if positive dist, 0 else
			double max_dist = 0;
			bool in_hull = true;
			for (int k = 0; k < n; ++k) {
				if (k == i || k == j) continue;
				// k is the index we're testing against the edge from i to j
				double dist = distance(points[k], points[i], points[j]);
				//cout << "dist " << dist << " at " << i << ":" << j << ":" << k << endl;
				int sign = (abs(dist)>0.0000001)?((dist>0)?1:-1):0;
				if (sign != 0 && sign == -working_sign) {
					in_hull = false;
					//cout << "not in hull: " << "(" << i << "," << j << ")" << endl;
					break;
				}
				working_sign = (sign?sign:working_sign);
				if (abs(dist) > max_dist) {
					max_dist = abs(dist);
				}
			}
			if (max_dist < min_width && in_hull) {
				//cout << "new min width: " << max_dist << endl;
				min_width = max_dist;
			}
		}
	}
	if (min_width == (1<<30)) {
		min_width = 0;
	}
	return min_width;
}

int main() {
	polys input = get_input();
	for (int i = 0; i < input.size(); ++i) {
		double res = process(input[i]);
		//long long rounded = round(res*100);
		cout << "Case " << (i+1) << ": ";
		printf("%.2f\n", res);
	}
}
