// General graph class

#include <vector>
#include <algorithm>
#include <limits>
#include <cassert>

using namespace std; // in a header, because fuck good practices

const float INF = std::numeric_limits<float>::infinity();

struct Edge {
	int other;
	float weight;

	Edge(int other, float weight)
		: other(other), weight(weight) {}
};

struct DoubleEdge {
	int src;
	int dst;
	float weight;

	DoubleEdge(int src, int dst, float weight)
		: src(src), dst(dst), weight(weight) {}
};

bool operator<(const Edge& lhs, const Edge& rhs) {
	return lhs.other < rhs.other;
}

vector<Edge>::const_iterator sorted_find(const vector<Edge>& v, int other) {
	return lower_bound(v.begin(), v.end(), Edge(other, 0));
}

vector<Edge>::iterator sorted_find(vector<Edge>& v, int other) {
	return lower_bound(v.begin(), v.end(), Edge(other, 0));
}

/** Inserts the given number into the vector, maintaining the invariant that it's a
  * sorted list of unique integers. Returns false if the element already exists.
*/
void sorted_insert(vector<Edge>& v, int other, float weight=1) {
	vector<Edge>::iterator it = sorted_find(v, other);
	if (it != v.end() && it->other == other) {
		it->weight = weight; // We're maintaining a set, so no duplicates allowed
	} else {
		v.insert(it, Edge(other, weight));
	}
}

void sorted_remove(vector<Edge>& v, int other) {
	vector<Edge>::iterator it = sorted_find(v, other);
	if (it != v.end() && it->other == other) {
		v.erase(it);
	}
}

/** Represents a graph where the vertices are labeled with the integers
  * 0, 1, 2, ... g.num_verts()-1
  * Supports quick insertion and querying of relationships and is relatively space-efficient.
  */
class Graph {
public:
	Graph(int size=0)
		: edges(size), edges_rev(size) {}

	Graph(const vector<DoubleEdge>& edge_list, bool undirected=false) {
		for (int i = 0; i < edge_list.size(); ++i) {
			DoubleEdge edge = edge_list[i];
			while (edge.src >= num_verts() || edge.dst >= num_verts()) {
				add_vertex();
			}
			edges[edge.src].push_back(Edge(edge.dst, edge.weight));
			edges_rev[edge.dst].push_back(Edge(edge.src, edge.weight));
			if (undirected) {
                edges[edge.dst].push_back(Edge(edge.src, edge.weight));
                edges_rev[edge.src].push_back(Edge(edge.dst, edge.weight));
            }
		}

		for (int i = 0; i < num_verts(); ++i) {
			sort(edges[i].begin(), edges[i].end());
			sort(edges_rev[i].begin(), edges_rev[i].end());
		}
	}

	void add_edge(int src, int dst, float weight=1) {
		assert(src >= 0 && dst >= 0 && src < num_verts() && dst < num_verts());
		sorted_insert(edges[src], dst, weight);
		sorted_insert(edges_rev[dst], src, weight);
	}

	void add_edge_undirected(int src, int dst, float weight=1) {
        add_edge(src, dst, weight);
        add_edge(dst, src, weight);
	}

	void remove_edge(int src, int dst) {
		assert(src >= 0 && dst >= 0 && src < num_verts() && dst < num_verts());
		sorted_remove(edges[src], dst);
		sorted_remove(edges_rev[dst], src);
	}

	int add_vertex() {
		edges.push_back(vector<Edge>());
		edges_rev.push_back(vector<Edge>());
		return num_verts() - 1;
	}

	int num_verts() const {
		return edges.size();
	}

	int num_edges() const {
		int total = 0;
		for (int i = 0; i < num_verts(); ++i) {
			total += edges[i].size();
		}
		return total;
	}

	double weight(int src, int dst, float _default=INF) const {
		assert(src >= 0 && dst >= 0 && src < num_verts() && dst < num_verts());
		vector<Edge>::const_iterator pos = sorted_find(edges[src], dst);
		if (pos == edges[src].end() || pos->other != dst) {
			return _default;
		}
		return pos->weight;
	}

	bool is_connected(int src, int dst) const {
		assert(src >= 0 && dst >= 0 && src < num_verts() && dst < num_verts());
		vector<Edge>::const_iterator pos = sorted_find(edges[src], dst);
		return pos != edges[src].end() && pos->other == dst;
	}

	const vector<Edge>& incoming(int dst) const {
		assert(dst >= 0 && dst < num_verts());
		return edges_rev[dst];
	}

	const vector<Edge>& outgoing(int src) const {
		assert(src >= 0 && src < num_verts());
		return edges[src];
	}

	vector<DoubleEdge> all_edges() const {
		vector<DoubleEdge> retval;
		for (int i = 0; i < num_verts(); ++i) {
			for (int j = 0; j < edges[i].size(); ++j) {
				retval.push_back(DoubleEdge(i, edges[i][j].other, edges[i][j].weight));
			}
		}
		return retval;
	}

	void reverse() {
		std::swap(edges, edges_rev);
	}

	vector<vector<Edge> > edges;
	vector<vector<Edge> > edges_rev;
};

#include <vector>
#include <list>

const float BIG = 1 << 20; // "infinity", but with subtraction working

bool clear_excess(const Graph& capacity, Graph& flow,
                  vector<int>& height, vector<float>& excess, int vertex) {
    // Repeatedly does pushes and relabels in order to remove the excess flow
    // in the given vertex.
    // Returns true if the vertex needed to be relabeled to clear it.

    if (excess[vertex] == 0) {
        return false;
    }

    const vector<Edge>& cap_incoming = capacity.incoming(vertex);
    const vector<Edge>& cap_outgoing = capacity.outgoing(vertex);
    const vector<Edge>& flow_incoming = flow.incoming(vertex);
    const vector<Edge>& flow_outgoing = flow.outgoing(vertex);

    int min_height_unsaturated_neighbor = 0x7fffffff;

    // Push all flow possible onto downhill, downstream edges.
    for (int i = 0; i < cap_outgoing.size(); ++i) {
        int other = cap_outgoing[i].other;
        float capacity = cap_outgoing[i].weight;
        float curr_flow = flow_outgoing[i].weight;

        // Keep track of max height of all adjacent, unsaturated vertices
        if (height[other] != height[vertex] - 1) {
            if (curr_flow < capacity && height[other] < min_height_unsaturated_neighbor) {
                min_height_unsaturated_neighbor = height[other];
            }
            continue;
        }

        // Push more along this edge, if possible
        if (curr_flow < capacity) {
            float extra_capacity = capacity - curr_flow;
            if (extra_capacity < excess[vertex]) {
                // Add as much flow as possible.
                excess[other] += extra_capacity;
                excess[vertex] -= extra_capacity;
                flow.add_edge(vertex, other, capacity);
            } else {
                // Put all of excess into this flow.
                float new_flow = curr_flow + excess[vertex];
                excess[other] += excess[vertex];
                excess[vertex] = 0;
                flow.add_edge(vertex, other, new_flow);
                return false;
            }
        }
    }
    // Push all flow possible back to downhill, upstream edges.
    for (int i = 0; i < cap_incoming.size(); ++i) {
        int other = cap_incoming[i].other;
        float curr_flow = flow_incoming[i].weight;

        // Keep track of max height of all adjacent, unsaturated vertices
        if (height[other] != height[vertex] - 1) {
            if (curr_flow > 0 && height[other] < min_height_unsaturated_neighbor) {
                min_height_unsaturated_neighbor = height[other];
            }
            continue;
        }

        // Reduce flow along this edge, if possible.
        if (curr_flow < excess[vertex]) {
            // Cancel all flow
            excess[other] += curr_flow;
            excess[vertex] -= curr_flow;
            flow.add_edge(other, vertex, 0);
        } else {
            // Cancel some flow, clearing all excess
            float new_flow = curr_flow - excess[vertex];
            excess[other] += excess[vertex];
            excess[vertex] = 0;
            flow.add_edge(other, vertex, new_flow);
            return false;
        }
    }

    // Pushing flow around couldn't clear the excess, so need to relabel and try again.
    height[vertex] = min_height_unsaturated_neighbor + 1;
    clear_excess(capacity, flow, height, excess, vertex);
    return true;
}

void make_flow_graph(Graph& g, int dst) {
    // Modifies the graph so that it's a valid flow graph:
    // 1. No antiparallel edges. Any antiparallel edges are replaced with
    //    and extra vertex.
    // 2. No self loops. They're just removed.
    // 3. No negative-weight edges. They're set to zero capacity.

    for (int v = 0; v < g.num_verts(); ++v) {
        for (int j = 0; j < g.outgoing(v).size(); ++j) {
            Edge e = g.outgoing(v)[j];
            if (e.weight < 0) {
                g.add_edge(v, e.other, 0);
            } else if (g.is_connected(e.other, v)) {
                int new_vertex = g.add_vertex();
                g.add_edge(e.other, new_vertex, e.weight);
                g.add_edge(new_vertex, v, e.weight);
                g.remove_edge(e.other, v);
            }
        }
        if (g.is_connected(v, v)) {
            g.remove_edge(v, v);
        }
    }
}

float max_flow(Graph capacity, int src, int dst) {
    make_flow_graph(capacity, dst);

    vector<float> excess (capacity.num_verts(), 0);
    vector<int> height (capacity.num_verts(), 0);
    height[src] = capacity.num_verts();

    Graph flow = capacity;
    for (int i = 0; i < flow.num_verts(); ++i) {
        const vector<Edge>& adj = flow.outgoing(i);
        for (int j = 0; j < adj.size(); ++j) {
            Edge e = adj[j];
            if (i == src) {
                float weight = capacity.weight(i, e.other);
                flow.add_edge(i, e.other, weight);
                excess[src] -= weight;
                excess[e.other] += weight;
            } else {
                flow.add_edge(i, e.other, 0);
            }
        }
    }

    list<int> discharge_queue;
    for (int v = 0; v < capacity.num_verts(); ++v) {
        if (v != src && v != dst) {
            discharge_queue.push_back(v);
        }
    }

sue_me:
    for(list<int>::iterator i = discharge_queue.begin(); i != discharge_queue.end(); ++i) {
        int v = *i;
        bool was_relabeled = clear_excess(capacity, flow, height, excess, v);
        if (was_relabeled) {
            discharge_queue.erase(i);
            discharge_queue.push_front(v);
            goto sue_me;
        }
    }

    return excess[dst];
}

#include <string>
#include <iostream>

vector<Graph> get_input() {
	int num_inputs;
	cin >> num_inputs;
	vector<Graph> retval;
	for (int count = 0; count < num_inputs; ++count) {
		int w, h;
		cin >> w >> h;
		int size = w*h;
		int outside_vert = size;
		int inside_vert = size+1;
		retval.push_back(Graph(size+2));
		cin.ignore(100000, '\n');
		vector<string> map (h);
		for (int j = 0; j < h; ++j) {
			getline(cin, map[j]);
		}

		// Got the map, now make the graph.
		Graph& g = retval.back();
		for (int row = 0; row < h; ++row) {
			for (int col = 0; col < w; ++col) {
				if (map[row][col] == 'X') {
					continue;
				}

				int edge_adj_count = 0;
				int drop_adj_count = 0;
				bool is_adj_top = false;
				bool is_adj_bot = false;
				bool is_adj_lef = false;
				bool is_adj_rig = false;
				if (row == 0) {
					++edge_adj_count;
				} else if (map[row-1][col] == 'D') {
					++drop_adj_count;
				} else if (map[row-1][col] == '.') {
					is_adj_top = true;
				}
				if (row == h-1) {
					++edge_adj_count;
				} else if (map[row+1][col] == 'D') {
					++drop_adj_count;
				} else if (map[row+1][col] == '.') {
					is_adj_bot = true;
				}
				if (col == 0) {
					++edge_adj_count;
				} else if (map[row][col-1] == 'D') {
					++drop_adj_count;
				} else if (map[row][col-1] == '.') {
					is_adj_lef = true;
				}
				if (col == w-1) {
					++edge_adj_count;
				} else if (map[row][col+1] == 'D') {
					++drop_adj_count;
				} else if (map[row][col+1] == '.') {
					is_adj_rig = true;
				}

				g.add_edge(outside_vert, inside_vert, g.weight(outside_vert, inside_vert)+edge_adj_count);
				if (map[row][col] == '.') {
					if (is_adj_top) {
						g.add_edge(row*h+col, (row-1)*h+col, 1);
					}
					if (is_adj_bot) {
						g.add_edge(row*h+col, (row+1)*h+col, 1);
					}
					if (is_adj_lef) {
						g.add_edge(row*h+col, row*h+col-1, 1);
					}
					if (is_adj_rig) {
						g.add_edge(row*h+col, row*h+col+1, 1);
					}
					g.add_edge(row*h+col, inside_vert, drop_adj_count);
					g.add_edge(outside_vert, row*h+col, edge_adj_count);
				}
			}
		}
	}

	return retval;
}

int main() {
	vector<Graph> input = get_input();
	for (int i = 0; i < input.size(); ++i) {
		cout << int(max_flow(input[i], input[i].num_verts()-2, input[i].num_verts()-1)) << endl;
	}
}
