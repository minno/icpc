#include <iostream>
#include <vector>

using namespace std;

struct prob {
	int num;
	int denom;
	int start;
	int end;
};

typedef vector<prob> vp;
typedef vector<int> vi;

vp get_input() {
	int num_probs;
	cin >> num_probs;
	vp input (num_probs);
	for (int i = 0; i < num_probs; ++i) {
		prob& p = input[i];
		cin >> p.num >> p.denom >> p.start >> p.end;
	}
	return input;
}

vi process(int num, int denom, int start, int end) {
	vi result;
	num %= denom;
	for (int i = 0; i < start; ++i) {
		num *= 7;
		// result.push_back(num / denom);
		num %= denom;
	}
	for (int i = start; i <= end; ++i) {
		num *= 7;
		result.push_back(num / denom);
		num %= denom;
	}
	return result;
}

int main() {
	vp input = get_input();
	for(int i = 0; i < input.size(); ++i) {
		vi result = process(input[i].num, input[i].denom, input[i].start, input[i].end);
		cout << "Problem set " << i+1 << ": " << input[i].num << " / " << input[i].denom <<
			", base 7 digits " << input[i].start << " through " << input[i].end << ": ";
		for (int j = 0; j < result.size(); ++j) {
			cout << result[j];
		}
		cout << '\n';
	}
}

