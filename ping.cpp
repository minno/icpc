#include <iostream>
#include <vector>
#include <string>

using namespace std;

vector<string> get_input() {
	vector<string> retval;
	string s;
	while (getline(cin, s) && s != "0") {
		retval.push_back(s);
	}
	return retval;
}

vector<int> solve(string& input) {
	int n = input.size();
	vector<bool> is_on (n, false);
	for (int i = 1; i < n; ++i) {
		int parity = 0;
		for (int j = 1; j < i; ++j) {
			if (is_on[j] && i%j == 0) {
				parity ^= 1;
			}
		}
		if (parity != input[i] - '0') {
			is_on[i] = true;
		}
	}
	vector<int> retval;
	for (int i = 0; i < n; ++i) {
		if (is_on[i]) {
			retval.push_back(i);
		}
	}
	return retval;
}

int main() {
	vector<string> input = get_input();
	for (int i = 0; i < input.size(); ++i) {
		vector<int> output = solve(input[i]);
		for (int j = 0; j < output.size(); ++j) {
			cout << (j==0?"":" ") << output[j];
		}
		cout << endl;
	}
}
