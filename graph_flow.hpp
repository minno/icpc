#pragma once

#include "graph.hpp"
#include <vector>
#include <list>

const float BIG = 1 << 20; // "infinity", but with subtraction working

bool clear_excess(const Graph& capacity, Graph& flow,
                  vector<int>& height, vector<float>& excess, int vertex) {
    // Repeatedly does pushes and relabels in order to remove the excess flow
    // in the given vertex.
    // Returns true if the vertex needed to be relabeled to clear it.

    if (excess[vertex] == 0) {
        return false;
    }

    const vector<Edge>& cap_incoming = capacity.incoming(vertex);
    const vector<Edge>& cap_outgoing = capacity.outgoing(vertex);
    const vector<Edge>& flow_incoming = flow.incoming(vertex);
    const vector<Edge>& flow_outgoing = flow.outgoing(vertex);

    int min_height_unsaturated_neighbor = 0x7fffffff;

    // Push all flow possible onto downhill, downstream edges.
    for (int i = 0; i < cap_outgoing.size(); ++i) {
        int other = cap_outgoing[i].other;
        float capacity = cap_outgoing[i].weight;
        float curr_flow = flow_outgoing[i].weight;

        // Keep track of max height of all adjacent, unsaturated vertices
        if (height[other] != height[vertex] - 1) {
            if (curr_flow < capacity && height[other] < min_height_unsaturated_neighbor) {
                min_height_unsaturated_neighbor = height[other];
            }
            continue;
        }

        // Push more along this edge, if possible
        if (curr_flow < capacity) {
            float extra_capacity = capacity - curr_flow;
            if (extra_capacity < excess[vertex]) {
                // Add as much flow as possible.
                excess[other] += extra_capacity;
                excess[vertex] -= extra_capacity;
                flow.add_edge(vertex, other, capacity);
            } else {
                // Put all of excess into this flow.
                float new_flow = curr_flow + excess[vertex];
                excess[other] += excess[vertex];
                excess[vertex] = 0;
                flow.add_edge(vertex, other, new_flow);
                return false;
            }
        }
    }
    // Push all flow possible back to downhill, upstream edges.
    for (int i = 0; i < cap_incoming.size(); ++i) {
        int other = cap_incoming[i].other;
        float curr_flow = flow_incoming[i].weight;

        // Keep track of max height of all adjacent, unsaturated vertices
        if (height[other] != height[vertex] - 1) {
            if (curr_flow > 0 && height[other] < min_height_unsaturated_neighbor) {
                min_height_unsaturated_neighbor = height[other];
            }
            continue;
        }

        // Reduce flow along this edge, if possible.
        if (curr_flow < excess[vertex]) {
            // Cancel all flow
            excess[other] += curr_flow;
            excess[vertex] -= curr_flow;
            flow.add_edge(other, vertex, 0);
        } else {
            // Cancel some flow, clearing all excess
            float new_flow = curr_flow - excess[vertex];
            excess[other] += excess[vertex];
            excess[vertex] = 0;
            flow.add_edge(other, vertex, new_flow);
            return false;
        }
    }

    // Pushing flow around couldn't clear the excess, so need to relabel and try again.
    height[vertex] = min_height_unsaturated_neighbor + 1;
    clear_excess(capacity, flow, height, excess, vertex);
    return true;
}

void make_flow_graph(Graph& g, int dst) {
    // Modifies the graph so that it's a valid flow graph:
    // 1. No antiparallel edges. Any antiparallel edges are replaced with
    //    and extra vertex.
    // 2. No self loops. They're just removed.
    // 3. No negative-weight edges. They're set to zero capacity.

    for (int v = 0; v < g.num_verts(); ++v) {
        for (int j = 0; j < g.outgoing(v).size(); ++j) {
            Edge e = g.outgoing(v)[j];
            if (e.weight < 0) {
                g.add_edge(v, e.other, 0);
            } else if (g.is_connected(e.other, v)) {
                int new_vertex = g.add_vertex();
                g.add_edge(e.other, new_vertex, e.weight);
                g.add_edge(new_vertex, v, e.weight);
                g.remove_edge(e.other, v);
            }
        }
        if (g.is_connected(v, v)) {
            g.remove_edge(v, v);
        }
    }
}

float max_flow(Graph capacity, int src, int dst) {
    make_flow_graph(capacity, dst);

    vector<float> excess (capacity.num_verts(), 0);
    vector<int> height (capacity.num_verts(), 0);
    height[src] = capacity.num_verts();

    Graph flow = capacity;
    for (int i = 0; i < flow.num_verts(); ++i) {
        const vector<Edge>& adj = flow.outgoing(i);
        for (int j = 0; j < adj.size(); ++j) {
            Edge e = adj[j];
            if (i == src) {
                float weight = capacity.weight(i, e.other);
                flow.add_edge(i, e.other, weight);
                excess[src] -= weight;
                excess[e.other] += weight;
            } else {
                flow.add_edge(i, e.other, 0);
            }
        }
    }

    list<int> discharge_queue;
    for (int v = 0; v < capacity.num_verts(); ++v) {
        if (v != src && v != dst) {
            discharge_queue.push_back(v);
        }
    }

sue_me:
    for(list<int>::iterator i = discharge_queue.begin(); i != discharge_queue.end(); ++i) {
        int v = *i;
        bool was_relabeled = clear_excess(capacity, flow, height, excess, v);
        if (was_relabeled) {
            discharge_queue.erase(i);
            discharge_queue.push_front(v);
            goto sue_me;
        }
    }

    return excess[dst];
}
