#include <iostream>
#include <vector>
#include <cstdio>
#include <cmath>

using namespace std;

vector<vector<double> > get_input() {
	int num_problems;
	cin >> num_problems;
	vector<vector<double> > retval (num_problems);
	for (int i = 0; i < num_problems; ++i) {
		int num_numbers;
		cin >> num_numbers;
		retval[i].reserve(num_numbers);
		for (int j = 0; j < num_numbers; ++j) {
			double d;
			cin >> d;
			retval[i].push_back(d);
		}
	}
	return retval;
}

double solve_brute(vector<double>& nums) {
	while (nums.size() > 1) {
		for (int i = 0; i < nums.size()-1; ++i) {
			nums[i] = .5 * (nums[i] + nums[i+1]);
		}
		nums.pop_back();
	}
	return nums[0];
}

double solve(vector<double>& nums) {
	vector<double> coeffs (nums.size());
	vector<int> div_2_counts (nums.size());

	int n = nums.size() - 1;

	double coeff = 1;
	int div_2_count = 0;

	for (int i = 0; i < (nums.size()+1)/2; ++i) {
		if (i != 0) {
			coeff *= (n+1 - i);
			coeff /= i;

			while (coeff > 1) {
				coeff /= 2;
				div_2_count++;
			}
		}

		coeffs[i] = coeff;
		div_2_counts[i] = div_2_count;
		coeffs[n - i] = coeff;
		div_2_counts[n - i] = div_2_count;
	}

	double acc = 0;

	for (int i = 0; i < nums.size(); ++i) {
		if (div_2_counts[i] < (n - 100)) {
			continue;
		}

		acc += nums[i] * coeffs[i] * pow(2, div_2_counts[i] - n);

	}

	return acc;
}

int main() {
	vector<vector<double> > nums = get_input();
	for (int i = 0; i < nums.size(); ++i) {
		printf("Case #%d: %.3f\n", i+1, solve(nums[i]));
	}
}
