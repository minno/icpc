#include <iostream>
#include <vector>
#include <utility>
#include <cmath>

using namespace std;

#define MAX (1<<30);

struct problem {
	int start;
	int end;

	problem(int s = 0, int e = 0) : start(s), end(e) {}
};

vector<problem> get_input() {
	int total_num;
	cin >> total_num;
	vector<problem> res (total_num);
	int i = 0;
	while (cin >> res[i].start >> res[i].end) {
		++i;
	}
	return res;
}

int num_steps(problem p, int prev_step = 1) {
	if (p.start < 0 || p.end < 0) {
		return -2;
	}
	if (p.start >= p.end) {
		return p.end - p.start;
	}
	if (prev_step <= 0) {
		return MAX;
	} else if (abs(p.end - p.start - prev_step) <= 1) {
		return 1;
	} else if (abs(p.end - p.start - 2*prev_step) <= 1) {
		return 2;
	} else if (p.end - p.start <= 2 * prev_step) {	
		return MAX;
	} else {
		return 2 + min(min(num_steps(problem(p.start + prev_step, p.end - prev_step), prev_step), 
			num_steps(problem(p.start + prev_step + 1, p.end- prev_step - 1), prev_step + 1)),
			num_steps(problem(p.start + prev_step - 1, p.end - prev_step + 1), prev_step - 1));
	}
}

int main() {
	vector<problem> input = get_input();
	for (int i = 0; i < input.size(); ++i) {
		cout << 2+num_steps(problem(input[i].start+1, input[i].end-1)) << endl;
	}
}
