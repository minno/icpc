#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <sstream>

using namespace std;

typedef vector<string> vec;

vec get_input() {
	vec result;
	string buf;
	getline(cin, buf); // consume extra line
	int num_lines, num_cols;
	stringstream sstream (buf);
	sstream >> num_lines >> num_cols;
	if (num_lines == 0 && num_cols == 0) {
		throw "Done with input.";
	}
	for (int cnt = 0; cnt < num_lines; ++cnt) {
		getline(cin, buf);
		for (int i = 0; i < buf.size(); ++i) {
			if (buf[i] == '.') {
				buf[i] = '0';
			}
		}
		result.push_back(buf);
	}
	return result;
}

void add_nums(vec& board, int mine_i, int mine_j) {
	int i_low_lim = max(0, mine_i-1);
	int i_high_lim = min(mine_i+1, int(board.size() - 1));
	int j_low_lim = max(0, mine_j-1);
	int j_high_lim = min(mine_j+1, int(board[0].size() - 1));
	for (int i = i_low_lim; i <= i_high_lim; ++i) {
		for (int j = j_low_lim; j <= j_high_lim; ++j) {
			if (board[i][j] != '*') {
				board[i][j]++;
			}
		}
	}
}

void process(vec& board) {
	for (int i = 0; i < board.size(); ++i) {
		for (int j = 0; j < board[i].size(); ++j) {
			if (board[i][j] == '*') {
				add_nums(board, i, j);
			}
		}
	}
}

void print_board(vec& board) {
	for (int i = 0; i < board.size(); ++i) {
		cout << board[i] << endl;
	}
}

int main() {
	int i = 1;
	try {
		while (1) {
			auto board = get_input();
			process(board);
			if (i != 1) {
				cout << endl;
			}
			cout << "Field #" << i++ << ":" << endl;
			print_board(board);
		}
	} catch (const char* msg) {
	}
}
