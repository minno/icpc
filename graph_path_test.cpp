#include "graph.hpp"
#include "graph_path.hpp"

#include <cstdio>
#include <vector>

void test_bf_success() {
    Graph g (4);
    g.add_edge(0, 1, 2);
    g.add_edge(0, 2, 50);
    g.add_edge(1, 3, 2);
    g.add_edge(2, 3, -50);
    vector<float> lens = bellman_ford(g, 0);
    printf("Test 1: bellman-ford success\n");
    printf("Distance to 0: %s\n", (lens[0] == 0 ? "correct" : "incorrect"));
    printf("Distance to 1: %s\n", (lens[1] == 2 ? "correct" : "incorrect"));
    printf("Distance to 2: %s\n", (lens[2] == 50 ? "correct" : "incorrect"));
    printf("Distance to 3: %s\n", (lens[3] == 0 ? "correct" : "incorrect"));
    printf("\n");
}

void test_bf_neg_cycle() {
    Graph g (3);
    g.add_edge(0, 1, 2);
    g.add_edge(1, 2, -2);
    g.add_edge(2, 0, -2);
    vector<float> lens = bellman_ford(g, 0);
    printf("Test 2: bellman-ford negative cycle detection\n");
    printf("Detected cycle? %s\n", (lens.size() == 0? "yes" : "no"));
    printf("\n");
}

void test_dijkstra() {
    Graph g (3);
    g.add_edge(0, 1, 1);
    g.add_edge(1, 2, 1);
    g.add_edge(0, 2, 3);
    vector<float> lens = dijkstra(g, 0);
    printf("Test 3: dijkstra's algorithm\n");
    printf("Distance to 0: %s\n", (lens[0] == 0 ? "correct" : "incorrect"));
    printf("Distance to 1: %s\n", (lens[1] == 1 ? "correct" : "incorrect"));
    printf("Distance to 2: %s\n", (lens[2] == 2 ? "correct" : "incorrect"));
    printf("\n");
}

void test_johnson_success() {
    Graph g (4);
    g.add_edge(0, 1, 2);
    g.add_edge(0, 2, 50);
    g.add_edge(1, 3, 2);
    g.add_edge(2, 3, -50);
    g.add_edge(3, 0, 7);
    vector<vector<float> > lens = johnson(g);
    printf("Test 4: johnson's algorithm\n");
    printf("Distance from 0 to 0: %s\n", (lens[0][0] == 0 ? "correct" : "incorrect"));
    printf("Distance from 0 to 1: %s\n", (lens[0][1] == 2 ? "correct" : "incorrect"));
    printf("Distance from 0 to 2: %s\n", (lens[0][2] == 50 ? "correct" : "incorrect"));
    printf("Distance from 0 to 3: %s\n", (lens[0][3] == 0 ? "correct" : "incorrect"));
    printf("Distance from 1 to 0: %s\n", (lens[1][0] == 9 ? "correct" : "incorrect"));
    printf("Distance from 1 to 1: %s\n", (lens[1][1] == 0 ? "correct" : "incorrect"));
    printf("Distance from 1 to 2: %s\n", (lens[1][2] == 59 ? "correct" : "incorrect"));
    printf("Distance from 1 to 3: %s\n", (lens[1][3] == 2 ? "correct" : "incorrect"));
    printf("Distance from 2 to 0: %s\n", (lens[2][0] == -43 ? "correct" : "incorrect"));
    printf("Distance from 2 to 1: %s\n", (lens[2][1] == -41 ? "correct" : "incorrect"));
    printf("Distance from 2 to 2: %s\n", (lens[2][2] == 0 ? "correct" : "incorrect"));
    printf("Distance from 2 to 3: %s\n", (lens[2][3] == -50 ? "correct" : "incorrect"));
    printf("Distance from 3 to 0: %s\n", (lens[3][0] == 7 ? "correct" : "incorrect"));
    printf("Distance from 3 to 1: %s\n", (lens[3][1] == 9 ? "correct" : "incorrect"));
    printf("Distance from 3 to 2: %s\n", (lens[3][2] == 57 ? "correct" : "incorrect"));
    printf("Distance from 3 to 3: %s\n", (lens[3][3] == 0 ? "correct" : "incorrect"));
    printf("\n");
}

int main() {
    test_bf_success();

    test_bf_neg_cycle();

    test_dijkstra();

    test_johnson_success();
}
