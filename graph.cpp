#include "graph.hpp"
#include <iostream>

int main() {
	Graph g (3);
	cout << g.num_edges() << endl;
	g.add_edge(0, 1);
	g.add_edge(1, 2);
	cout << g.num_edges() << endl;
	g.add_edge(1, 2);
	cout << g.num_edges() << endl;

	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			cout << i << " -> " << j << ": " << g.weight(i, j) << endl;
		}
	}
}
