#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

struct point{
  double x;
  double y;
  bool in_hull;
};

typedef vector<point> polygon

polygon get_a_polygon() {
  int num_points;
  cin >> num_points;
  vp vertices (num_points);
  for (int i = 0; i < num_points; ++i) {
    point& p = vertices[i];
    cin >> p.x >> p.y;
    p.in_hull = true;
  }
  return vertices;
}

vector<polygon> get_input() {
  polygon example;
  vector<polygon> input
   do {
     example = get_a_polygon();
     input.push_back(example);
   } while (example.size() != 0);
  return input;
}

double find_distance(point end_1, point end_2, point outlier) {
  double m = (end_1.y - end_2.y)/(end_1.x - end_2.x);
  double b = end_1.y - m*end_1*x;
  double answer = (outlier.y - m*outlier.x - b)/(sqrt(m*m + 1));
  return answer;
}

double solve(polygon trash) {
  double diameter;
  int edges = trash.size();
  for (int i = 0; i < edges ; i++) {
    point pt = trash[i];
    int adj = (i + 1) % edges;
    point adj_pt = trash[adj];
    int two_out = (i + 2) % edges;
    point two_out_pt = trash[two_out];
    double max_diam;
    double min_diam;
    bool adj_in = true;
    for (int j = 2; j < edges - 1; j++) {
      int vert_coord = (i + j) % edges;
      point vert = trash[vert_coord];
      if (!vert.in_hull) {
	continue;
      }
      double dist = find_distance(pt,adj,vert);
      if (max_diam < dist) {
	max_diam = dist;
      } else if (min_diam > dist) {
	min_diam = dist;
      }
      if (max_diam > max_diam + min_diam) {
	adj_in = false;
      }
    
    
    
