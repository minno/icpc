#include "graph.hpp"
#include "graph_flow.hpp"

#include <cstdio>

void dump_state(const Graph& flow, const vector<int>& height, const vector<float>& excess) {
    printf("Flow:\n");
    for (int i = 0; i < flow.num_verts(); ++i) {
        for (int j = i+1; j < flow.num_verts(); ++j) {
            float curr_flow = flow.weight(i, j, 0);
            if (curr_flow > 0) {
                printf("\t%d -> %d: %f\n", i, j, curr_flow);
            }
        }
    }

    printf("Heights:\n");
    for (int i = 0; i < height.size(); ++i) {
        printf("\t%d: %d\n", i, height[i]);
    }

    printf("Excesses:\n");
    for (int i = 0; i < excess.size(); ++i) {
        printf("\t%d: %f\n", i, excess[i]);
    }
    printf("\n");
}

void test_clear_excess() {
    Graph capacity(5);
    capacity.add_edge(0, 1, 12);
    capacity.add_edge(0, 2, 14);
    capacity.add_edge(1, 2, 5);
    capacity.add_edge(1, 4, 16);
    capacity.add_edge(2, 3, 8);
    capacity.add_edge(3, 1, 7);
    capacity.add_edge(3, 4, 10);

    Graph flow(5);
    flow.add_edge(0, 1, 12);
    flow.add_edge(0, 2, 14);
    flow.add_edge(1, 2, 0);
    flow.add_edge(1, 4, 0);
    flow.add_edge(2, 3, 0);
    flow.add_edge(3, 1, 0);
    flow.add_edge(3, 4, 0);

    vector<int> height(5, 0);
    height[0] = 5;

    vector<float> excess(5, 0);
    excess[0] = -26;
    excess[1] = 12;
    excess[2] = 14;

    clear_excess(capacity, flow, height, excess, 1);
    clear_excess(capacity, flow, height, excess, 2);
    clear_excess(capacity, flow, height, excess, 1);
    clear_excess(capacity, flow, height, excess, 3);

    printf("clear_excess working correctly? %s\n", (excess[4] == 20 ? "yes" : "no"));
}

void test_max_flow() {
    Graph capacity(5);
    capacity.add_edge(0, 1, 12);
    capacity.add_edge(0, 2, 14);
    capacity.add_edge(1, 2, 5);
    capacity.add_edge(1, 4, 16);
    capacity.add_edge(2, 3, 8);
    capacity.add_edge(3, 1, 7);
    capacity.add_edge(3, 4, 10);

    float flow = max_flow(capacity, 0, 4);

    printf("max_flow working correctly? %s\n", (flow == 20 ? "yes" : "no"));
}

void test_max_flow2() {
    Graph capacity(6);
    capacity.add_edge(0, 1, 12);
    capacity.add_edge(0, 4, 10);
    capacity.add_edge(1, 3, 20);
    capacity.add_edge(1, 4, 9);
    capacity.add_edge(2, 0, 16);
    capacity.add_edge(2, 4, 13);
    capacity.add_edge(4, 0, 4);
    capacity.add_edge(4, 5, 14);
    capacity.add_edge(5, 1, 7);
    capacity.add_edge(5, 3, 4);

    float flow = max_flow(capacity, 2, 3);

    printf("max_flow still working correctly? %s\n", (flow == 23 ? "yes" : "no"));
}

int main() {
    test_clear_excess();
    test_max_flow();
    test_max_flow2();
}
