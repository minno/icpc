#pragma once

// Single-source and all-pairs shortest path algorithms

#include "graph.hpp"
#include <vector>

vector<float> bellman_ford(const Graph& g, int src) {
    // Returns a list of the path lengths to all other vertices if are no
    // negative-weight cycles, or an empty vector if there is one.

    vector<float> old_lens(g.num_verts(), INF);
    old_lens[src] = 0;
    vector<float> new_lens = old_lens;

    for (int count = 0; count < g.num_verts()+1; ++count) {
        old_lens = new_lens;
        for (int v = 0; v < g.num_verts(); ++v) {
            const vector<Edge> incoming = g.incoming(v);
            for (int i = 0; i < incoming.size(); ++i) {
                Edge e = incoming[i];
                float new_len = old_lens[e.other] + e.weight;
                if (new_len < new_lens[v]) {
                    new_lens[v] = new_len;
                }
            }
        }
    }

    // If there aren't any negative-weight cycles, then the last iteration
    // should have had no effect. If it did, then there must be one.
    for (int i = 0; i < old_lens.size(); ++i) {
        if (old_lens[i] != new_lens[i]) {
            old_lens.clear();
            break;
        }
    }
    return old_lens;
}

struct Element {
    float priority;
    int index;

    Element(float priority = -1, int index = -1)
        : priority(priority), index(index) {}
};

class DijkstraMinHeap {
public:
    vector<Element> heap;
    vector<int> indices;

    DijkstraMinHeap(int size)
        : heap(size), indices(size) {
        for (int i = 0; i < size; ++i) {
            heap[i] = Element(INF, i);
            indices[i] = i;
        }
    }

    void heap_swap(int index1, int index2) {
        swap(heap[index1], heap[index2]);
        index1 = heap[index1].index;
        index2 = heap[index2].index;
        swap(indices[index1], indices[index2]);
    }

    void siftdown(int index) {
        int child1 = 2*index+1;
        int child2 = 2*index+2;
        int next_child;
        if (child1 >= heap.size()) {
            return;
        } else if (child2 >= heap.size()) {
            next_child = child1;
        } else if (heap[child1].priority < heap[child2].priority) {
            next_child = child1;
        } else {
            next_child = child2;
        }

        if (heap[next_child].priority > heap[index].priority) {
            return;
        }

        heap_swap(index, next_child);
        siftdown(next_child);
    }

    void siftup(int index) {
        int parent = (index-1) / 2;
        if (index == 0 || heap[parent].priority <= heap[index].priority) {
            return;
        }

        heap_swap(parent, index);
        siftup(parent);
    }

    Element pop() {
        if (heap.size() == 0) {
            return Element(-1, -1);
        }
        heap_swap(0, heap.size()-1);
        Element top = heap.back();
        heap.pop_back();
        indices[top.index] = -1;
        siftdown(0);
        return top;
    }

    bool empty() {
        return heap.empty();
    }

    // Returns false if the attempted opration was done to an already-popped entry.
    bool decrease_priority(int index, float new_priority) {
        int heap_index = indices[index];
        if (heap_index == -1) {
            return false;
        }

        if (new_priority > heap[heap_index].priority) {
            return true;
        }

        heap[heap_index].priority = new_priority;
        siftup(heap_index);
        return true;
    }
};

vector<float> dijkstra(const Graph& g, int src) {
    DijkstraMinHeap heap(g.num_verts());
    heap.decrease_priority(src, 0);
    for (int i = 0; i < g.outgoing(src).size(); ++i) {
        Edge e = g.outgoing(src)[i];
        heap.decrease_priority(e.other, e.weight);
    }

    vector<float> retval (g.num_verts());
    vector<bool> is_filled (g.num_verts(), false);

    while (!heap.empty()) {
        Element e = heap.pop();
        int vertex = e.index;
        float distance = e.priority;
        retval[vertex] = distance;
        is_filled[vertex] = true;

        const vector<Edge>& outgoing = g.outgoing(vertex);
        for (int i = 0; i < outgoing.size(); ++i) {
            Edge edge = outgoing[i];
            if (!is_filled[edge.other]) {
                heap.decrease_priority(edge.other, distance + edge.weight);
            }
        }
    }
    return retval;
}

vector<vector<float> > johnson(const Graph& g) {
    vector<vector<float> > retval;

    Graph bf_graph (g);
    bf_graph.add_vertex();
    for (int i = 0; i < g.num_verts(); ++i) {
        bf_graph.add_edge(g.num_verts(), i, 0);
    }

    vector<float> bf_result = bellman_ford(bf_graph, g.num_verts());
    if (bf_result.empty()) {
        return retval;
    }

    Graph dijkstra_graph (g);
    for (int i = 0; i < dijkstra_graph.num_verts(); ++i) {
        const vector<Edge>& adj_list = dijkstra_graph.outgoing(i);
        for (int j = 0; j < adj_list.size(); ++j) {
            int src = i;
            int dst = adj_list[j].other;
            float new_weight = adj_list[j].weight + bf_result[src] - bf_result[dst];
            dijkstra_graph.add_edge(src, dst, new_weight);
        }
    }

    for (int src = 0; src < g.num_verts(); ++src) {
        vector<float> dijkstra_result = dijkstra(dijkstra_graph, src);
        for (int dst = 0; dst < dijkstra_result.size(); ++dst) {
            dijkstra_result[dst] += bf_result[dst] - bf_result[src];
        }
        retval.push_back(dijkstra_result);
    }

    return retval;
}
