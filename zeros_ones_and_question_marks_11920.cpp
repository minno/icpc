#include <iostream>
#include <vector>
#include <string>

using namespace std;

struct run {
	char type;
	int num;
};

typedef vector<run> vr;

vector<vr> get_input() {
	int num_inputs;
	cin >> num_inputs;
	vector<vr> result (num_inputs);
	for (int i = 0; i < num_inputs; ++i) {
		vr& runs = result[i];
		string buf;
		cin >> buf;
		char cur = buf[0];
		int cur_run_len = 1;
		for (int j = 1; j < buf.length(); ++j) {
			if (buf[j] == cur) {
				cur_run_len++;
			} else {
				run r;
				r.type = cur;
				r.num = cur_run_len;
				runs.push_back(r);
				cur = buf[j];
				cur_run_len = 1;
			}
		}
		run r;
		r.type = cur;
		r.num = cur_run_len;
		runs.push_back(r);
	}
	return result;
}

int solve(vr& runs) {
	if (runs.size() == 1) {
		if (runs[0].type == '?') {
			return 1;
		} else {
			return runs[0].num;
		}
	}
	if (runs.front().type == '?') {
		runs.pop_front();
	}
	if (runs.back().type == '?') {
		runs.pop_back();
	}
	int max_run_len = 0;
	for (int i = 0; i < runs.size(); ++i) {
		if (runs[i].type != '?' && runs[i].num > max_run_len) {
			max_run_len = runs[i].num;
		}
	}
	for (int i = 1; i < runs.size()-1; /* no incr */) {
		if (runs[i].type == '?' && (runs[i-1].type == runs[i+1].type) == (runs[i].num % 2 != 0)) {
			runs.erase(runs.begin() + i);
		} else {
			++i;
		}
	}
	

int main() {
	vector<vr> input = get_input();
	for (int i = 0; i < input.size(); ++i) {
		int res = solve(input[i]);
		printf("Case %d: %d", i+1, res);
	}
}
