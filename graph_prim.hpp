#pragma once

// Prim's algorithm
#include "graph.hpp"
#include <queue>
#include <vector>

struct WeightGt {
	bool operator()(const DoubleEdge& lhs, const DoubleEdge& rhs) {
		return lhs.weight > rhs.weight;
	}
};

// Returns a vector of the edges making up a minimum spanning tree of g
vector<DoubleEdge> prim_mst(const Graph& g) {
	vector<bool> visited (g.num_verts(), false);

	priority_queue<DoubleEdge, vector<DoubleEdge>, WeightGt> edge_heap;
	vector<DoubleEdge> retval;

	// Special case for the first vertex, since there is no "connected region" yet
	for (int i = 0; i < g.outgoing(0).size(); ++i) {
		Edge e = g.outgoing(0)[i];
		edge_heap.push(DoubleEdge(0, e.other, e.weight));
	}
	visited[0] = true;

	for (int count = 1; count < g.num_verts(); ++count) {
		DoubleEdge e (0, 0, 0);
		while (visited[e.dst]) {
			e = edge_heap.top();
			edge_heap.pop();
		}
		const vector<Edge>& new_edges = g.outgoing(e.dst);
		for (int i = 0; i < new_edges.size(); ++i) {
			if (!visited[new_edges[i].other]) {
				edge_heap.push(DoubleEdge(e.dst, new_edges[i].other, new_edges[i].weight));
			}
		}
		visited[e.dst] = true;
		retval.push_back(e);
	}

	return retval;
}
