#include <iostream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

#define INT_MAX 0x7FFFFFFF;

enum Turn {
	RIGHT,
	LEFT,
	END,
};

struct Move {
	int distance;
	Turn turn;
};

vector<vector<Move> > get_input() {
	string line;
	vector<vector<Move> > retval;
	while (getline(cin, line)) {
		if (line[0] == 'X') {
			return retval;
		}
		stringstream ss(line);
		retval.push_back(vector<Move>());
		int distance;
		char turn;
		while (ss >> distance >> turn) {
			Move m;
			m.distance = distance;
			switch (turn) {
				case 'L':
					m.turn = LEFT;
					break;
				case 'R':
					m.turn = RIGHT;
					break;
				case 'X':
					m.turn = END;
					break;
				default:
					cerr << "unexpected character: " << turn << '\n';
			}
			retval.back().push_back(m);
		}
	}
	return retval;
}

struct Line {
	int x1;
	int x2;
	int y1;
	int y2;

	Line(int x1, int x2, int y1, int y2)
		: x1(x1), x2(x2), y1(y1), y2(y2) {}
};

enum Facing {
	NORTH,
	EAST,
	SOUTH,
	WEST
};

vector<Line> get_lines(vector<Move>& moves) {
	int minx = INT_MAX;
	int miny = INT_MAX;
	int x=0, y=0;
	int xp, yp;
	Facing f = EAST;
	vector<Line> retval;
	for (int i = 0; i < moves.size(); ++i) {
		xp = x;
		yp = y;
		switch (f) {
			case NORTH:
				y -= moves[i].distance;
				break;
			case SOUTH:
				y += moves[i].distance;
				break;
			case WEST:
				x -= moves[i].distance;
				break;
			case EAST:
				x += moves[i].distance;
				break;
		}
		if (x < minx) {
			minx = x;
		}
		if (y < miny) {
			miny = y;
		}
		if (moves[i].turn == RIGHT) {
			f = Facing((f+1)%4);
		} else if (moves[i].turn == LEFT) {
			f = Facing((f+4-1)%4);
		}
	}

	x = -minx;
	y = -miny;
	f = EAST;

	for (int i = 0; i < moves.size(); ++i) {
		xp = x;
		yp = y;
		switch (f) {
			case NORTH:
				y -= moves[i].distance;
				break;
			case SOUTH:
				y += moves[i].distance;
				break;
			case WEST:
				x -= moves[i].distance;
				break;
			case EAST:
				x += moves[i].distance;
				break;
		}
		if (x < minx) {
			minx = x;
		}
		if (y < miny) {
			miny = y;
		}
		if (moves[i].turn == RIGHT) {
			f = Facing((f+1)%4);
		} else if (moves[i].turn == LEFT) {
			f = Facing((f+4-1)%4);
		}
		retval.push_back(Line(xp, x, yp, y));
	}

	return retval;
}

void print_lines(const vector<Line>& lines) {
	vector<string> buf;
	for (int row_num = 0; row_num < lines.size(); ++row_num) {
		Line l = lines[row_num];
		l.x1 *= 2;
		l.x2 *= 2;
		l.y1 *= 2;
		l.y2 *= 2;
		if (max(l.x1, l.x2) >= buf.size()) {
			buf.resize(max(l.x1, l.x2)+1);
		}
		bool is_horizontal = (l.x1 == l.x2);
		if (is_horizontal) {
			string& row = buf[l.x1];
			int start = min(l.y1, l.y2);
			int end = max(l.y1, l.y2);
			if (row.size() <= end+1) {
				row.resize(end+1, ' ');
			}
			for (int i = start; i <= end; ++i) {
				if (row[i] != ' ') {
					row[i] = '+';
				} else {
					row[i] = '-';
				}
			}
		} else {
			int start = min(l.x1, l.x2);
			int end = max(l.x1, l.x2);
			int col_num = l.y1;
			if (buf.size() <= end+1) {
				buf.resize(end+1);
			}
			for (int i = start; i <= end; ++i) {
				if (buf[i].size() <= col_num) {
					buf[i].resize(col_num+1, ' ');
				}
				if (buf[i][col_num] != ' ') {
					buf[i][col_num] = '+';
				} else {
					buf[i][col_num] = '|';
				}
			}
		}
	}
	
	for (int i = 0; i < buf.size(); ++i) {
		cout << buf[i] << '\n';
	}
}

int main() {
	vector<vector<Move> > input = get_input();
	for (int i = 0; i < input.size(); ++i) {
		cout << "Case " << (i+1) << ":\n";
		print_lines(get_lines(input[i]));
		cout << endl;
	}
}
